#1) cargar las librerias
library("affy")
library("limma")
library("genefilter")
library("GEOquery")
library("ggplot2")
library("pheatmap")
library("phantasus")
library("dplyr")
library("hgu133plus2.db")

#cargar paquetes
BiocManager::install(c("limma", "edgeR", "Glimma", "org.Mm.eg.db", "gplots", "RColorBrewer"))
if (!requireNamespace("BiocManager", quietly = TRUE))
  install.packages("BiocManager")
BiocManager::install("hgu133plus2.db")
BiocManager::install("GEOquery")
BiocManager::install("limma")
BiocManager::install("pheatmap")
BiocManager::install("org.Hs.eg.db")
install.packages("xlsx")


#2) importar targets
#para las celulas KOPT-K1
targetsK <- readTargets("targetsK.txt", row.names="FileName")
#para las celulas HPB-ALL
targetsH <- readTargets("targetsH.txt", row.names="FileName")
#importar targets para ambas lineas celulares
targets <- readTargets("targets.txt", row.names="FileName")

#3) importar data
#para las celulas KOPT-K1
dataK <- ReadAffy(filenames=targetsK$FileName) 
#para las celulas HPB-ALL
dataH <- ReadAffy(filenames=targetsH$FileName)
#para ambas lineas celulares
data <- ReadAffy(filenames=targets$FileName) 

#4) Normalizar con RMA 
#para celulas KOPT-K1
esetK <- expresso(dataK,
                  bg.correct = TRUE, 
                  bgcorrect.method="rma",
                  normalize = TRUE, 
                  normalize.method="quantiles", 
                  pmcorrect.method="pmonly", 
                  summary.method="medianpolish",
                  verbose = TRUE
) 
#para celulas HPB-ALL
esetH <- expresso(dataH,
                  bg.correct = TRUE, 
                  bgcorrect.method="rma",
                  normalize = TRUE, 
                  normalize.method="quantiles", 
                  pmcorrect.method="pmonly", 
                  summary.method="medianpolish",
                  verbose = TRUE
) 
#para ambas lineas celulares
eset <- expresso(data,
                 bg.correct = TRUE, 
                 bgcorrect.method="rma",
                 normalize = TRUE, 
                 normalize.method="quantiles", 
                 pmcorrect.method="pmonly", 
                 summary.method="medianpolish",
                 verbose = TRUE
)

#crear values para matriz de ambos
TS <- paste(targets$Strain, targets$Treatment, sep=".") #hacer tratamiento y starin un solo vector


#5) Filtrar data utilizandi IQR.
#para celulas KOPT-K1
esetIQRK <- varFilter(esetK, var.func=IQR, var.cutoff=0.5, filterByQuantile=TRUE)
colnames(esetIQRK) <- row.names(targetsK)
head(exprs(esetIQRK))
expression.dataset.K <- exprs(esetIQRK)
#para celulas HPB-ALL
esetIQRH <- varFilter(esetH, var.func=IQR, var.cutoff=0.5, filterByQuantile=TRUE)
colnames(esetIQRH) <- row.names(targetsH)
head(exprs(esetIQRH))
expression.dataset.H <- exprs(esetIQRH)
#para ambas lineas celulares
esetIQR <- varFilter(eset, var.func=IQR, var.cutoff=0.5, filterByQuantile=TRUE)
colnames(esetIQR) <- row.names(targets)
head(exprs(esetIQR))
View(exprs(esetIQR))
expression.dataset <- exprs(esetIQR)

#6) Diseñar Matríz 
#para celulas KOPT-K1
designK<-cbind(DSMO=c(1,1,1,0,0,0), SAHM1=c(0,0,0,1,1,1))
rownames(designK)<-targetsK$FileName
#para celulas HPB-ALL
designH<-cbind(DSMO=c(1,1,1,0,0,0), SAHM1=c(0,0,0,1,1,1))
rownames(designH)<-targetsH$FileName
#para ambas lineas celulares
design<-cbind(KOP.Control=c(1,1,1,0,0,0,0,0,0,0,0,0),
              HPB.Control=c(0,0,0,1,1,1,0,0,0,0,0,0), 
              KOP.Prueba=c(0,0,0,0,0,0,1,1,1,0,0,0),
              HPB.Prueba=c(0,0,0,0,0,0,0,0,0,1,1,1))
rownames(design)<-targets$FileName


#7) Matriz de contraste
#para celulas KOPT-K1
cont.matrixK<-makeContrasts(DSMOvsSAHM1=DSMO-SAHM1,levels=designK) 
#para celulas HPB-ALL
cont.matrixH<-makeContrasts(DSMOvsSAHM1=DSMO-SAHM1,levels=designH) 
#para ambas lineas celulares
cont.matrix <- makeContrasts(ControlvsPruebainKOP=KOP.Control-KOP.Prueba, ControlvsPruebainHPB=HPB.Control-HPB.Prueba, 
                             Diff=(KOP.Control-KOP.Prueba)-(HPB.Control-HPB.Prueba), levels=design)

#8) Obtener differentially expressed genes (DEGs)
#Linear model and eBayes para celulas KOPT-K1
fitK<-lmFit(esetIQRK,designK)  ##getting DEGs from IQR 
fit2K<-contrasts.fit(fitK, cont.matrixK)
fit2K<-eBayes(fit2K)
toptableIQRK<-topTable(fit2K, number=dim(exprs(esetIQRK))[1], adjust.method="BH", sort.by="p") #crear tabla
save(toptableIQRK,file="ResultadosK.RData")

#Linear model and eBayes para celulas HPB-ALL
fitH<-lmFit(esetIQRH,designH)  ##getting DEGs from IQR 
fit2H<-contrasts.fit(fitH, cont.matrixH)
fit2H<-eBayes(fit2H)
toptableIQRH<-topTable(fit2H, number=dim(exprs(esetIQRH))[1], adjust.method="BH", sort.by="p") #crear tabla
save(toptableIQRH,file="ResultadosH.RData")

#para ambas lineas celulares
fit <- lmFit(eset, design)
fit<-eBayes(fit)
fit2 <- contrasts.fit(fit, cont.matrix)
fit2 <- eBayes(fit2)
toptableIQR<-topTable(fit2, number=dim(exprs(esetIQR))[1], adjust.method="BH", sort.by="F")
save(toptableIQR,file="ResultadosGlobal.RData")
toptableIQR

#9) Anotaciones, filtrando por genes significativos
#para celulas HPB-ALL
load("ResultadosH.RData")                                                           
library("hgu133plus2.db")     
ID.fdr.005.table.H<-subset(toptableIQRH, toptableIQRH$adj.P.Val<=0.05)
ID.fdr.005.table.H
probenames.fdr.005.H<-as.character(rownames(ID.fdr.005.table.H))
probenames.fdr.005.H
list.GeneSymbol.fdr.005.H<-mget(probenames.fdr.005.H, hgu133plus2SYMBOL)
char.GeneSymbol.fdr.005.H<- as.character(list.GeneSymbol.fdr.005.H)
toptable.annotated.HPB<-cbind(ID.fdr.005.table.H,char.GeneSymbol.fdr.005.H)
toptable.annotated.HPB
full_results.HPB <- tibble::rownames_to_column(toptable.annotated.HPB,"ID")
full_results.HPB

#para celulas KOPT-K1
load("ResultadosK.RData")    
library("hgu133plus2.db")
ID.fdr.005.table.K<-subset(toptableIQRK, toptableIQRK$adj.P.Val<=0.05)
ID.fdr.005.table.K
probenames.fdr.005.K<-as.character(rownames(ID.fdr.005.table.K))
probenames.fdr.005.K
list.GeneSymbol.fdr.005.K<-mget(probenames.fdr.005.K, hgu133plus2SYMBOL)
char.GeneSymbol.fdr.005.K<- as.character(list.GeneSymbol.fdr.005.K)
toptable.annotated.KOPT<-cbind(ID.fdr.005.table.K,char.GeneSymbol.fdr.005.K)
toptable.annotated.KOPT
full_results.KOPT <- tibble::rownames_to_column(toptable.annotated.KOPT,"ID")
full_results.KOPT

#Ambas lineas celulares
load("ResultadosGlobal.RData")                                                           
library("hgu133plus2.db")     
ID.fdr.005.table<-subset(toptableIQR, toptableIQR$adj.P.Val<=0.05)
ID.fdr.005.table
probenames.fdr.005<-as.character(rownames(ID.fdr.005.table))
probenames.fdr.005
list.GeneSymbol.fdr.005<-mget(probenames.fdr.005, hgu133plus2SYMBOL)
char.GeneSymbol.fdr.005<- as.character(list.GeneSymbol.fdr.005)
toptable.annotated<-cbind(ID.fdr.005.table,char.GeneSymbol.fdr.005)
toptable.annotated
full_results <- tibble::rownames_to_column(toptable.annotated,"ID")
full_results

#Renombrar columnas
colnames(full_results.HPB) <- c("ID","logFC","AveExpr","t","P.Value","adj.P.Val","B","Gene")
full_results.HPB
colnames(full_results.KOPT) <- c("ID","logFC","AveExpr","t","P.Value","adj.P.Val","B","Gene")
full_results.KOPT
colnames(full_results) <- c("ID","logFC","AveExpr","t","P.Value","adj.P.Val","B","Gene")
full_results

#10) Filtrado: Significativos y downregulated
library(readr)
install.packages("xlsx") #no linux
library("xlsx") #no linux

results_filtered.K<-filter(full_results.KOPT, adj.P.Val < 0.05, logFC < 0)
write.xlsx(results_filtered.K, file="results_filtered.K.xlsx", append = FALSE)
results_filtered.H<-filter(full_results.HPB, adj.P.Val < 0.05, logFC < 0)
write.xlsx(results_filtered.H, file="results_filtered.H.xlsx", append = FALSE)

#ranking
#todas
toptable.annotated$logadjustp <- -log10(toptable.annotated$adj.P.Val)
toptable.annotated$rank <- NA
order.logpadj <- order(-toptable.annotated$logadjustp)
toptable.annotated$rank[order.logpadj] <- 1:nrow(toptable.annotated)
toptable.annotated$logadjustp <- toptable.annotated$logadjustp * ifelse(toptable.annotated$ControlvsPruebainKOP < 0,-1,1)

#H
toptable.annotated.HPB$logadjustp <- -log10(toptable.annotated.HPB$adj.P.Val)
toptable.annotated.HPB$rank <- NA
order.logpadj <- order(-toptable.annotated.HPB$logadjustp)
toptable.annotated.HPB$rank[order.logpadj] <- 1:nrow(toptable.annotated.HPB)
toptable.annotated.HPB$logadjustp <- toptable.annotated.HPB$logadjustp * ifelse(toptable.annotated.HPB$logFC < 0,-1,1)

#K
toptable.annotated.KOPT$logadjustp <- -log10(toptable.annotated.KOPT$adj.P.Val)
toptable.annotated.KOPT$rank <- NA
order.logpadj <- order(-toptable.annotated.KOPT$logadjustp)
toptable.annotated.KOPT$rank[order.logpadj] <- 1:nrow(toptable.annotated.KOPT)
toptable.annotated.KOPT$logadjustp <- toptable.annotated.KOPT$logadjustp * ifelse(toptable.annotated.KOPT$logFC < 0,-1,1)


####################Visualizaciones###
###############################################################################################################
#11)Boxplots
#Boxplot para ambas lineas celulares previa normalizacion
colors = c(rep("#00BFC4",3),rep("#F8766D",3),rep("#C77CFF",3), rep("#7CAE00",3))
boxplot(data,
        main="Boxplot antes de la Normalización",
        names=c("KOP-DSMO-1","KOP-DSMO-2","KOP-DSMO-3","HPB-DSMO-1","HPB-DSMO-2","HPB-DSMO-3",
                "KOP-SAHM1-1","KOP-SAHM1-2","KOP-SAHM1-3","HPB-SAHM1-1","HPB-SAHM1-2","HPB-SAHM1-3"),
        las=2, col = colors, cex.axis=0.7, cex.title=1)

#Boxplot para ambas lineas celulares post normalizacion
exprseset <- as.data.frame(exprs(eset))   
boxplot(data.frame(exprseset),
        main="Boxplot depués de la Normalización",
        names=c("KOP-DSMO-1","KOP-DSMO-2","KOP-DSMO-3","HPB-DSMO-1","HPB-DSMO-2","HPB-DSMO-3",
                "KOP-SAHM1-1","KOP-SAHM1-2","KOP-SAHM1-3","HPB-SAHM1-1","HPB-SAHM1-2","HPB-SAHM1-3"),
        las=2, col = colors, cex.axis=0.7, cex.title=1)

#12)Agrupación de muestras y PCA
library(ggplot2)
library(ggrepel)
library(ggthemes)

#Heatmap
library(pheatmap)
corMatrix <- cor(exprs(eset),use="c")
pheatmap(corMatrix) 
rownames(targets)
colnames(corMatrix)
## If not, force the rownames to match the columns
rownames(targets) <- colnames(corMatrix)
pheatmap(corMatrix,
         annotation_col=targets)  

### Classify cell strains into 4 groups
## Add a column called 'type':
targetsPCA <- as.data.frame(targets)
targetsPCA <- targetsPCA %>% mutate(type=ifelse((Strain == 'KOP-K1' & Treatment=='DSMO'), "KOP-K1-DSMO",
                                                ifelse((Strain == 'HPB-ALL' & Treatment=='DSMO'), "HPB-ALL-DSMO",
                                                       ifelse((Strain == 'KOP-K1' & Treatment=='SAHM1'), "KOP-K1-SAHM1", "HPB-ALL-SAHM1"))))
targetsPCA
## ggplot theme
theme_new <- theme_set(theme_bw())
theme_new <- theme_update(
  legend.position = "bottom")
pca <- prcomp(t(exprs(eset)))
cbind(targetsPCA, pca$x) %>% 
  ggplot(aes(x = PC1, y=PC2, col=type,label=paste("Cell line:", Strain))) + xlab('PCA1') + ylab('PCA2') + geom_point() + geom_text_repel()

#13)Volcanoplot
library(ggplot2)
volcanoplotH <- ggplot(toptableIQRH,aes(x = logFC, y=B)) + geom_point()
volcanoplotK <- ggplot(toptableIQRK,aes(x = logFC, y=B)) + geom_point()

##Marcando significativos
p_cutoff <- 0.05
fc_cutoff <- 1
volcanoplotHpro <-toptableIQRH %>% 
  mutate(Significant = adj.P.Val < p_cutoff, abs(logFC) > fc_cutoff ) %>% 
  ggplot(aes(x = logFC, y = B, col=Significant)) + geom_point()
volcanoplotKpro <-toptableIQRK %>% 
  mutate(Significant = adj.P.Val < p_cutoff, abs(logFC) > fc_cutoff ) %>% 
  ggplot(aes(x = logFC, y = B, col=Significant)) + geom_point()
volcanoplotHpro
volcanoplotKpro


#14) HeatMap
library(pheatmap)
topN <- 50
#para celulas KOPT-K1
ids_of_interest.K <- mutate(full_results.KOPT, Rank = 1:n()) %>% 
  filter(Rank < topN) %>% 
  pull(ID) 
gene_names.K <- mutate(full_results.KOPT, Rank = 1:n()) %>% 
  filter(Rank < topN) %>% 
  pull(Gene) 
gene_matrix.K <- exprs(esetK)[ids_of_interest.K,]
pheatmap.K <- pheatmap(gene_matrix.K,labels_row = gene_names.K)
pheatmap.row.K <- pheatmap(gene_matrix.K,
                           labels_row = gene_names.K,
                           scale="row")

#para celulas HPB-ALL
ids_of_interest.H <- mutate(full_results.HPB, Rank = 1:n()) %>% 
  filter(Rank < topN) %>% 
  pull(ID) 
gene_names.H <- mutate(full_results.HPB, Rank = 1:n()) %>% 
  filter(Rank < topN) %>% 
  pull(Gene) 
gene_matrix.H <- exprs(esetK)[ids_of_interest.H,]
pheatmap.H <- pheatmap(gene_matrix.H, labels_row = gene_names.H)
pheatmap.row.H <- pheatmap(gene_matrix.H,
                           labels_row = gene_names.H,
                           scale="row")


#15)diagrama de Venn
# Load library
install.packages("VennDiagram")

# Generate 3 sets of 200 words
set1 <- paste(rep("HPB" , 200) , sample(c(1:1000) , 200 , replace=F) , sep="")
set2 <- paste(rep("KOPT" , 200) , sample(c(1:1000) , 200 , replace=F) , sep="")


# Prepare a palette of 3 colors with R colorbrewer:
library(RColorBrewer)
myCol <- brewer.pal(3, "Pastel2")

# Chart
venn.diagram(
  x = list(set1, set2, set3),
  category.names = c("Set 1" , "Set 2 "),
  filename = '#14_venn_diagramm.png',
  output=TRUE,
  
  # Output features
  imagetype="png" ,
  height = 480 , 
  width = 480 , 
  resolution = 300,
  compression = "lzw",
  
  # Circles
  lwd = 2,
  lty = 'blank',
  fill = myCol,
  
  # Numbers
  cex = .6,
  fontface = "bold",
  fontfamily = "sans",
  
  # Set names
  cat.cex = 0.6,
  cat.fontface = "bold",
  cat.default.pos = "outer",
  cat.pos = c(-27, 27, 135),
  cat.dist = c(0.055, 0.055, 0.085),
  cat.fontfamily = "sans",
  rotation = 1
)